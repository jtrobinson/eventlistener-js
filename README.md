# README #


* 
* EventListener.js is a small function that assigns an event or Event object to an HTMLElement.
* It will use the standard addEventListener but will also detect older versions of
* IE and fall back to using attachEvent method or the onX (where X is the event name) property.
* 
* Version 1.0
*

### How do I use it? ###

* Download the file, set it up on your server and link to it as you normally would any
*     other JavaScript library. There are two methods that can be called:
*
*     In header or footer of page:
*     <script async src="EventListener.js"></script>
*
*     For example of use, see the [wiki page](https://bitbucket.org/jtrobinson/eventlistener.js/wiki).
*

### Created By ###

*
* Jim Robinson ( james.t.robinson.iii@techie-jim.net )
*