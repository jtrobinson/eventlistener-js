/*!
 * EventListener.js
 *
 * Author: James T. Robinson III (james.t.robinson.iii@techie-jim.net)
 * Summary: Cross-browser event listener addition and removal based on
 *          Diego Perini's contentloaded.js
 * Updated: 2014-01-15
 * License: MIT
 * Version: 1.0
 * Tags: ECMAScript, JavaScript, js, uncompressed
 *
 * URL:
 * http://www.techie-jim.net/
 *
 */

var EventListener = (function() {

    var add = document.body.addEventListener ? 'addEventListener' : 'attachEvent',
        rem = document.body.addEventListener ? 'removeEventListener' : 'detachEvent',
        pre = document.body.addEventListener ? '' : 'on',
        supportsEvents = typeof Event !== 'undefined',
        sanitizeEvent = function(e) {
            var eType = typeof e;
            if(eType === 'string' || !supportsEvents) {
                return pre + e.toString();
            } else if (e instanceof Event) {
                return e;
            }
        };

    // @target DOMElement reference
    // @event Event or event name string
    // @fn function reference
    return {
        'add': function(target, event, fn) {
            target[add](sanitizeEvent(event), fn);
        },
        'remove': function(target, event, fn) {
            target[rem](sanitizeEvent(event), fn);
        }
    };

}());